//
//  AppDelegate.h
//  FiberApp
//
//  Created by A. J. on 23/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

