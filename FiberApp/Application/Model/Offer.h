//
//  Offer.h
//  FiberApp
//
//  Created by A. J. on 24/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Offer : NSObject
@property (nonatomic, strong)NSString *title;
@property (nonatomic, strong)NSString *teaser;
@property (nonatomic, strong)NSURL  *thumbnailHiresUrl;
@property (nonatomic, strong)NSDecimalNumber *payout;

@end
