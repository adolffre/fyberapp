//
//  OfferViewController.h
//  FiberApp
//
//  Created by A. J. on 24/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Offer.h"
@interface OfferViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UILabel *lblNoOffers;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

#pragma mark - Properties

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property(nonatomic, strong)NSArray *offers;

@end
