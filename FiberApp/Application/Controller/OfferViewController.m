//
//  OfferViewController.m
//  FiberApp
//
//  Created by A. J. on 24/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import "OfferViewController.h"

@implementation OfferViewController
#pragma mark - Controller Life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    //notifications - listeners
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"getDataSuccess"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"getDataError"
                                               object:nil];
    
    
    [self setupActInd];
}

#pragma mark - Custom Methods
-(void)dissmissViewController{
    [[self navigationController ] popToRootViewControllerAnimated:YES];
}
-(void)reloadData{
    [self.tableView reloadData];
}
#pragma mark - ActivityIndicator
- (void)setupActInd
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicator.bounds = CGRectMake(0, 0, screenWidth , screenHeight);
    _activityIndicator.color = [UIColor darkGrayColor];
    _activityIndicator.center = self.view.center;
    _activityIndicator.alpha = 0.5f;
    [self.view addSubview:_activityIndicator];
    [_activityIndicator startAnimating];
}
#pragma mark - Notifications
- (void)receiveNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@"getDataSuccess"]) {
        self.offers = (NSArray *)notification.object;
        BOOL noOffers = self.offers.count==0?YES:NO;
        self.tableView.hidden = noOffers;
        self.lblNoOffers.hidden = !noOffers;
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    }
    else if ([[notification name] isEqualToString:@"getDataError"]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                                 message:@"Could not get  offers from server."
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 [self dissmissViewController];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];

    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [_activityIndicator stopAnimating];
        
    });
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.offers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *CellIdentifier = @"offerTableViewCellid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier];
    }
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:4];
    imageView.image = nil;
    
    Offer *offer = [self.offers objectAtIndex:indexPath.row];


    UILabel *lblTitle  = (UILabel *)[cell viewWithTag:1];
    UILabel *lblTeaser = (UILabel *)[cell viewWithTag:2];
    UILabel *lblPayout = (UILabel *)[cell viewWithTag:3];
    
    lblTitle.text = offer.title;
    lblTeaser.text = offer.teaser;
    lblPayout.text = [NSString stringWithFormat:@"%@",offer.payout];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:offer.thumbnailHiresUrl];
        dispatch_async(dispatch_get_main_queue(), ^{
            imageView.image = [UIImage imageWithData:imageData];
            imageView.alpha = 1.0f;
        });
    });

    return cell;
}




@end
