//
//  InitialViewController.h
//  FiberApp
//
//  Created by A. J. on 23/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Manager.h"
#import "OfferViewController.h"

@interface InitialViewController : UIViewController

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UITextField *txtUid;
@property (weak, nonatomic) IBOutlet UITextField *txtAPIKey;
@property (weak, nonatomic) IBOutlet UITextField *txtAppid;

#pragma mark - Properties

@property(nonatomic, strong)NSArray *offers;
@property(nonatomic, strong)Manager *manager;

#pragma mark - Actions

- (IBAction)btnSearchPressed:(id)sender;

@end
