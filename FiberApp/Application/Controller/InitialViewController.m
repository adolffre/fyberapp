//
//  InitialViewController.m
//  FiberApp
//
//  Created by A. J. on 23/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import "InitialViewController.h"

@implementation InitialViewController
#pragma mark - Controller Life cycle 
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.txtUid.text = @"spiderman";
    self.txtAPIKey.text = @"1c915e3b5d42d05136185030892fbb846c278927";
    self.txtAppid.text = @"2070";
    
    
}

#pragma mark - Actions
- (IBAction)btnSearchPressed:(id)sender {
    self.manager = [[Manager alloc] initWithUid:self.txtUid.text
                                       aPIKey:self.txtAPIKey.text
                                     andAppid:self.txtAppid.text];
    [self.manager getData];
 [self performSegueWithIdentifier:@"pushOffersId" sender:self];
}
@end
