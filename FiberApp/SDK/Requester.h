//
//  Requester.h
//  FiberApp
//
//  Created by A. J. on 23/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HashCreator.h"

@interface Requester : NSObject
#pragma mark - Properties
@property (nonatomic)NSMutableURLRequest *request;
@property (nonatomic)NSString *APIKey;
@property (nonatomic)NSArray *jsonArray;
#pragma mark - Methods
-(id)initWithUrl:(NSString *)url andAPIKey:(NSString *)APIKey;
-(BOOL)verifySignature:(NSHTTPURLResponse *)response
               andData:(NSData *)data;
-(void)requestDataWithCompletionHandler:(void (^)(BOOL valid))callback;
@end
