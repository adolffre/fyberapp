//
//  HashCreator.m
//  FiberApp
//
//  Created by A. J. on 23/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import "HashCreator.h"


@implementation HashCreator
#pragma mark - Methods
-(id)initWithString:(NSString *)str{
    self = [super init];
    if (self != nil) {
        //create hash usging sha1 
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        uint8_t digest[CC_SHA1_DIGEST_LENGTH];
        
        CC_SHA1(data.bytes, (CC_LONG)data.length, digest);
        
        NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
        
        for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        {
            [output appendFormat:@"%02x", digest[i]];
        }
        self.hashString = output;
    }
        return self;
}

@end
