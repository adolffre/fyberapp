
//
//  Manager.m
//  FiberApp
//
//  Created by A. J. on 23/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import "Manager.h"


@implementation Manager
#pragma mark - Methods
- (id) initWithUid:(NSString *)uid
            aPIKey:(NSString *)APIKey
          andAppid:(NSString *)appid {
    self = [super init];
    if (self != nil) {
        
        self.parameters = [@{
                             @"format" : @"json",
                             @"appid" : appid,
                             @"uid" : uid,
                             @"apple_idfa" : self.getIDFA,
                             @"offer_types" : @"112",
                             @"locale" : @"DE",                             
                             @"ip" : @"109.235.143.113",
                             @"os_version" : [[UIDevice currentDevice] systemVersion],
                             @"timestamp" : [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]
                             } mutableCopy];
        
        self.APIKey = APIKey;
        self.apiUrl = @"http://api.fyber.com/feed/v1/offers.json";
    }
    return self;
}
- (NSString *)getIDFA
{
    if([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]) {
        NSUUID *IDFA = [[ASIdentifierManager sharedManager] advertisingIdentifier];
        return [IDFA UUIDString];
    }
    return nil;
}
- (NSString *)requestParameters {
    //alphabetical order parameters
    NSArray *keys = [self.parameters.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSString *value = [NSString stringWithFormat:@"%@=%@",keys[0],self.parameters[keys[0]]];
    for (int i=1; i < keys.count; i++) {
        value = [value stringByAppendingFormat:@"&%@=%@",keys[i],self.parameters[keys[i]]];
    }
    return value;
 
}
- (void)getData{
    //get data from server
    NSString *parameters = self.requestParameters;
    HashCreator *hc = [[HashCreator alloc] initWithString:[NSString stringWithFormat:@"%@&%@",parameters, self.APIKey]];
    
    NSString *url = [self.apiUrl stringByAppendingFormat:@"?%@&hashkey=%@",parameters,hc.hashString];
    Requester *requester = [[Requester alloc] initWithUrl:url andAPIKey:self.APIKey];
    [requester requestDataWithCompletionHandler:^(BOOL valid) {
        if(valid){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"getDataSuccess"
                                                            object:[self parseOfferArray: requester.jsonArray]];
        }
        else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getDataError"
                                                                object:nil];
            
        }
    }];
    
}

-(NSArray *)parseOfferArray:(NSArray *)array{
    //parse array in Offer objects
    NSMutableArray *offers = [NSMutableArray new];
    for (NSDictionary *offerDic in array) {
        Offer *offer = [Offer new];
        offer.teaser = [offerDic objectForKey:@"teaser"];
        offer.title  = [offerDic objectForKey:@"title"];
        NSDictionary *thumbDic = [offerDic objectForKey:@"thumbnail"];
        offer.thumbnailHiresUrl  = [NSURL URLWithString:[thumbDic objectForKey:@"hires"]];
        offer.payout  = [offerDic objectForKey:@"payout"];
        [offers addObject:offer];
    }
    
    return [NSArray arrayWithArray:offers];
}
@end
