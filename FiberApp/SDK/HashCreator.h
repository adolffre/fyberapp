//
//  HashCreator.h
//  FiberApp
//
//  Created by A. J. on 23/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface HashCreator : NSObject
#pragma mark - Properties
@property(nonatomic) NSString *hashString;
#pragma mark - Methods
-(id)initWithString:(NSString *)str;

@end
