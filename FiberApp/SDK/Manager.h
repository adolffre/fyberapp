//
//  Manager.h
//  FiberApp
//
//  Created by A. J. on 23/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AdSupport/AdSupport.h>
#import <UIKit/UIKit.h>
#import "HashCreator.h"
#import "Requester.h"
#import "Offer.h"

@interface Manager : NSObject
#pragma mark - Properties
@property (nonatomic) NSMutableDictionary *parameters;
@property (nonatomic) NSArray *offers;
@property (nonatomic) NSString *apiUrl;
@property (nonatomic) NSString *APIKey;
#pragma mark - Methods
- (id) initWithUid:(NSString *)uid
            aPIKey:(NSString *)APIKey
          andAppid:(NSString *)appid;
- (void )getData;
- (NSString *)requestParameters ;
- (NSString *)getIDFA;
@end
