//
//  Requester.m
//  FiberApp
//
//  Created by A. J. on 23/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import "Requester.h"

@implementation Requester
-(id)initWithUrl:(NSString *)url andAPIKey:(NSString *)APIKey{
    self = [super init];
    if (self != nil) {
        self.request = [[NSMutableURLRequest alloc] init];
        [self.request setHTTPMethod:@"GET"];
        [self.request setURL:[NSURL URLWithString:url]];
        [self setAPIKey:APIKey];
    }
    return  self;
}
-(void)requestDataWithCompletionHandler:(void (^)(BOOL valid))callback{
    
    //request to server
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:self.request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                    if ((!error) && (httpResponse.statusCode == 200)) {
                        //without errors
                        NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
                        
                        if ([self verifySignature:urlResponse andData:data]) {
                        //valid signature
                            NSError *err = nil;
                            NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data
                                                                                 options:NSJSONReadingMutableContainers
                                                                                   error:&err];
                            if (jsonDic) {
                                //getting data
                                if([jsonDic objectForKey:@"count"]>0){
                                    self.jsonArray = [jsonDic objectForKey:@"offers"];
                                }
                                else{
                                    self.jsonArray = [NSArray new];
                                }
                                //no errors
                                callback(YES);
                                
                            }
                        }
                    }else{
                        //error getting data
                        callback(NO);
                      
                    }        
    }];
    [task resume];
    
}
-(BOOL)verifySignature:(NSHTTPURLResponse *)response
               andData:(NSData *)data{
    // verify if Signature is equal with hash
    NSString *signatureName = @"X-Sponsorpay-Response-Signature";
    NSString *signature;
    BOOL isEqual = NO;
    for (NSString *field in [response allHeaderFields] ) {
        
        if([field isEqualToString:signatureName]){
            signature = response.allHeaderFields[signatureName];
            NSString *body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSString *sendHash = [NSString stringWithFormat:@"%@%@",body,self.APIKey];
            
            HashCreator *hc = [[HashCreator alloc] initWithString:sendHash];
            
            if ([signature isEqualToString:hc.hashString]) {
                isEqual = YES;
            }
            break;
        }
    }
    
    return isEqual;
}

@end
