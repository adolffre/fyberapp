//
//  RequesterTest.m
//  FiberApp
//
//  Created by A. J. on 24/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Requester.h"
#import "Manager.h"


@interface RequesterTest : XCTestCase

@end

@implementation RequesterTest

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testCreateRequest {
    Requester *request = [[Requester alloc] initWithUrl:@"someValue" andAPIKey:@"someValueKey"];
    
    XCTAssertTrue([request.APIKey isEqualToString:@"someValueKey"]);
    XCTAssertTrue([request.request.HTTPMethod isEqualToString:@"GET"]);
    XCTAssertTrue([request.request.URL.absoluteString isEqualToString:@"someValue"]);
    
}
-(void)testVerifySignatureValid{
    Requester *req = [[Requester alloc] initWithUrl:@"http://api.fyber.com/feed/v1/offers.json?appid=2070&apple_idfa=316D6F17-CD0E-4E3F-85FB-495D52AE71F9&format=json&ip=109.235.143.113&locale=DE&offer_types=112&os_version=9.0&timestamp=1443050692.825973&uid=spiderman&hashkey=b8a5b2b3582c295dc9b3954ff49ea7ec79059a11"
                                              andAPIKey:@"1c915e3b5d42d05136185030892fbb846c278927"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:req.request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
                    XCTAssertTrue([req verifySignature:urlResponse andData:data]);
                    
                }] resume];

}
-(void)testVerifySignatureInvalid{
    Requester *req = [[Requester alloc] initWithUrl:@"invalidUrl"
                                          andAPIKey:@"invalidAPIKey"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:req.request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
                    XCTAssertFalse([req verifySignature:urlResponse andData:data]);
  
                }] resume];
    
}
-(void)testRequestDataValid{
    
    Requester *req = [[Requester alloc] initWithUrl:@"http://api.fyber.com/feed/v1/offers.json?appid=2070&apple_idfa=316D6F17-CD0E-4E3F-85FB-495D52AE71F9&format=json&ip=109.235.143.113&locale=DE&offer_types=112&os_version=9.0&timestamp=1443050692.825973&uid=spiderman&hashkey=b8a5b2b3582c295dc9b3954ff49ea7ec79059a11"
                                          andAPIKey:@"1c915e3b5d42d05136185030892fbb846c278927"];
    [req requestDataWithCompletionHandler:^(BOOL valid) {
        XCTAssertTrue(valid);
        
    }];
    
}
-(void)testRequestDataInvalid{
    
    Requester *req = [[Requester alloc] initWithUrl:@"invalidURL"
                                          andAPIKey:@"invalidAPIKey"];
    [req requestDataWithCompletionHandler:^(BOOL valid) {
        XCTAssertFalse(valid);
        
    }];

}


@end
