//
//  HashCreatorTest.m
//  FiberApp
//
//  Created by A. J. on 24/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HashCreator.h"

@interface HashCreatorTest : XCTestCase

@end

@implementation HashCreatorTest

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testCreateHash {
    HashCreator *hc = [[HashCreator alloc] initWithString:@"testHash"];
    
    XCTAssertTrue([hc.hashString isEqualToString:@"dffa28a916b20e540b2f6f9e4cdf66bfd01443f9"]);    
    
}



@end
