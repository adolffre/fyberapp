//
//  ManagerTest.m
//  FiberApp
//
//  Created by A. J. on 24/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Manager.h"
@interface ManagerTest : XCTestCase

@end

@implementation ManagerTest

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testCreateManager {
    Manager *manager = [[Manager alloc] initWithUid:@"spiderman"
                                       aPIKey:@"1c915e3b5d42d05136185030892fbb846c278927"
                                     andAppid:@"2070"];

    XCTAssertTrue([manager.APIKey isEqualToString:@"1c915e3b5d42d05136185030892fbb846c278927"]);
    XCTAssertTrue([manager.apiUrl isEqualToString:@"http://api.fyber.com/feed/v1/offers.json"]);
    XCTAssertTrue([[manager.parameters objectForKey:@"format"] isEqualToString:@"json"]);
    XCTAssertTrue([[manager.parameters objectForKey:@"appid"] isEqualToString:@"2070"]);
    XCTAssertTrue([[manager.parameters objectForKey:@"uid"] isEqualToString:@"spiderman"]);
    XCTAssertTrue([[manager.parameters objectForKey:@"apple_idfa"] isEqualToString:manager.getIDFA]);
    XCTAssertTrue([[manager.parameters objectForKey:@"offer_types"] isEqualToString:@"112"]);
    XCTAssertTrue([[manager.parameters objectForKey:@"locale"] isEqualToString:@"DE"]);
    XCTAssertTrue([[manager.parameters objectForKey:@"ip"] isEqualToString:@"109.235.143.113"]);
    XCTAssertTrue([[manager.parameters objectForKey:@"os_version"] isEqualToString:[[UIDevice currentDevice] systemVersion]]);
    XCTAssertNotNil([manager.parameters objectForKey:@"timestamp"]);
}
- (void)testOrderingParameters {
    NSString *expetedString = @"a=testeA&b=testeB&c=testeC";
    Manager *manager = [Manager new];
    manager.parameters = [@{@"c":@"testeC", @"b":@"testeB" , @"a": @"testeA"} mutableCopy];
    
    XCTAssertTrue([expetedString isEqualToString:manager.requestParameters]);
}
@end
